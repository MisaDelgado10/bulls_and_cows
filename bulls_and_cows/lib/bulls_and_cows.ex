defmodule BullsAndCows do
  def score_guess(secret, guess) do
    secret = String.graphemes(secret)
    guess = String.graphemes(guess)
    count_bulls_and_cows(secret, guess, 0,0)
  end

  def count_bulls_and_cows([],[], bull_num, cow_num) when bull_num == 4 do
    "You win"
  end

  def count_bulls_and_cows([],[], bull_num, cow_num) do
    "#{bull_num} Bulls, #{cow_num} Cows"
  end

  def count_bulls_and_cows(secret,guess,bull_num, cow_num) do
    cond  do
      hd(secret) == hd(guess) ->
        count_bulls_and_cows(tl(secret), tl(guess), bull_num + 1, cow_num)
      hd(secret) in guess and hd(guess) in secret->
        count_bulls_and_cows(tl(secret), tl(guess), bull_num, cow_num + 2)
      hd(secret) in guess ->
        count_bulls_and_cows(tl(secret), tl(guess), bull_num, cow_num + 1)
      hd(guess) in secret ->
        count_bulls_and_cows(tl(secret), tl(guess), bull_num, cow_num + 1)
      true -> #If it's not any of the previous cases then we cant to continue splitting the list
        count_bulls_and_cows(tl(secret), tl(guess), bull_num, cow_num)
    end
  end
end
